#!/bin/bash

SRC_DIR=/opt/k2data/src/
set -ex

# lib下面所有jar包的绝对路径.
jarsDirWithAbsoluteDir=$(ls -d ${SRC_DIR}/lib/*)
# 将所有jar包的绝对路径用':'隔开, 以符合classpath的格式.
libJarClasspath=$( echo "$jarsDirWithAbsoluteDir" | awk 'BEGIN { ORS=":"; } { print $1; }' )
java -cp ${SRC_DIR}/k2-bo-platform-api.jar:${libJarClasspath} com.k2data.bo.platform.Application