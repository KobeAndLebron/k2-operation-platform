#!/bin/bash

# 描述: 编译源代码(包括集成测试的源代码); 然后跑单元测试, 跳过集成测试; 最后将此项目的相关依赖安装到本地Maven库.

set -ex
SCRIPT_DIR=$(dirname $(readlink -e $0))

mvn --also-make -f ${SCRIPT_DIR}/../pom.xml -pl k2-bo-platform-api clean install -Dskip.ut=false -Dskip.it=true