#!/bin/bash

# 描述: 跑此项目的集成测试; 在此脚本执行之前, 需要执行"pre-build.sh"脚本的情况:
#       1. 本地Maven仓库没有此项目的相关依赖.
#       2. 本地已经有此项目的相关依赖, 但是依赖的项目代码有更新.
#   除了这两种情况, 其余情况都不需要执行"pre-build.sh".

SCRIPT_DIR=$(dirname $(readlink -e $0))
set -ex

mvn -f ${SCRIPT_DIR}/../pom.xml -pl k2-bo-platform-api clean integration-test -Dskip.ut=true