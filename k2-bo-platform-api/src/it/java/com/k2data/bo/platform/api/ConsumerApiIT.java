package com.k2data.bo.platform.api;

import com.k2data.bo.platform.dao.entity.Consumer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ConsumerApiIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getConsumer() {
        Consumer consumer = this.restTemplate.getForObject("/consumer/1", Consumer.class);
        Assert.assertNotNull(consumer);
    }

}