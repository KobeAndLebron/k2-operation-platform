package com.k2data.bo.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * Created by ChenJingShuai on 18-3-25.
 */
@SpringBootConfiguration
@ComponentScans({@ComponentScan("com.k2data.bo.platform.api"),
        @ComponentScan("com.k2data.bo.platform.dao"), @ComponentScan("com.k2data.bo.platform.service")})
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
