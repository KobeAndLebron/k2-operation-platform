package com.k2data.bo.platform.api;

import com.k2data.bo.platform.dao.entity.Consumer;
import com.k2data.bo.platform.service.ConsumerService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ChenJingShuai on 18-3-25.
 */
@RestController
@EnableAutoConfiguration
public class ConsumerApi {

    private final ConsumerService CONSUMER_SERVICE;

    public ConsumerApi(ConsumerService CONSUMER_SERVICE) {
        this.CONSUMER_SERVICE = CONSUMER_SERVICE;
    }

    @GetMapping("/consumer/{ID}")
    public Consumer getConsumer(@PathVariable("ID") String id) {
        return CONSUMER_SERVICE.getConsumerById(id);
    }
}
