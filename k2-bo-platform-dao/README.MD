### 主要使用注解的方式, 使用xml配置的原因:    

由于注解"@Results"不支持引用的形式, 即不支持复用;      
而XML配置的方式可以通过ID对ResultMap进行复用. 见下例:

-  注解配置的形式:

```java
@Select("SELECT * FROM STUDENTS WHERE STUD_ID=#{studId}")
@Results({
  @Result(id=true, column="stud_id", property="studId"),
  @Result(column="name", property="name"),
  @Result(column="email", property="email"),
  @Result(column="addr_id", property="address.addrId")
})
Student findStudentById(int studId);

@Select("SELECT * FROM STUDENTS")
@Results({
  @Result(id=true, column="stud_id", property="studId"),
  @Result(column="name", property="name"),
  @Result(column="email", property="email"),
  @Result(column="addr_id", property="address.addrId")
})
List<Student> findAllStudents();
```

- XML配置的形式:

```xml
...

<mapper namespace="com.mybatis3.mappers.StudentMapper">

    <select id="findAllStudents" resultMap="StudentResult">
        SELECT * FROM STUDENTS
    </select>
    <select id="findStudentById" resultMap="StudentResult">
        SELECT * FROM STUDENTS WHERE STUD_ID=#{studId}
    </select>
    
    
    <resultMap type="Student" id="StudentResult">
        <id property="studId" column="stud_id"/>
        <result property="name" column="name"/>
        <result property="email" column="email"/>
        <result property="phone" column="phone"/>
    </resultMap>
</mapper>

...
```

为了解决这个问题, 采取XML和注解混用的形式:

- XML的配置:


```xml
...

<mapper namespace="com.mybatis3.mappers.StudentMapper">
    
    <resultMap type="Student" id="StudentResult">
        <id property="studId" column="stud_id"/>
        <result property="name" column="name"/>
        <result property="email" column="email"/>
        <result property="phone" column="phone"/>
    </resultMap>
    
</mapper>
...
```

- 注解配置的形式:
 

```java
@Select("SELECT * FROM STUDENTS WHERE STUD_ID=#{studId}")
@ResultMap("com.mybatis3.mappers.StudentMapper.StudentResult")
Student findStudentById(int studId);

@Select("SELECT * FROM STUDENTS")
@ResultMap("com.mybatis3.mappers.StudentMapper.StudentResult")
List<Student> findAllStudents();

```


