package com.k2data.bo.platform.dao;

import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:application-dao.properties")
public @interface ApplicationContextLocation {
}
