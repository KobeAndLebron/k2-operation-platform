package com.k2data.bo.platform.dao.mapper;

import com.k2data.bo.platform.dao.ApplicationContextLocation;
import com.k2data.bo.platform.dao.entity.Consumer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ApplicationContextLocation
@MybatisTest
public class ConsumerMapperIT {

    @Autowired
    private ConsumerMapper consumerMapper;

    @Test
    public void getOne() {
        Consumer queryConsumer = new Consumer();
        queryConsumer.setId(1L);
        Consumer actualConsumer = this.consumerMapper.getOne(queryConsumer);
        Consumer expectedConsumer = new Consumer();
        expectedConsumer.setId(1L);
        expectedConsumer.setDeleted(false);
        expectedConsumer.setEmail("1005871591@qq.com");
        expectedConsumer.setPassword("123456");
        expectedConsumer.setIndustry("电网");
        Assert.assertEquals(expectedConsumer, actualConsumer);

        System.out.println(actualConsumer);
    }

    @Test
    public void insert() {
        Consumer consumer = new Consumer();
        consumer.setId(3L);
        consumer.setEmail("1005871593@qq.com");
        consumer.setPassword("323456");
        consumer.setIndustry("电网");
        this.consumerMapper.insert(consumer);

        Consumer queryConsumer = new Consumer();
        queryConsumer.setId(3L);
        Consumer actualConsumer = this.consumerMapper.getOne(queryConsumer);
        Assert.assertEquals(consumer, actualConsumer);

        this.consumerMapper.delete(queryConsumer);
        Assert.assertEquals(null, this.consumerMapper.getOne(queryConsumer));
    }

    @Test
    public void getList() {
        List<Consumer> allConsumerList = this.consumerMapper.getList(null);
        Assert.assertEquals(allConsumerList.size(), 2);
    }

}