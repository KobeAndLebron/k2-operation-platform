DROP TABLE IF EXISTS `consumers`;
CREATE TABLE IF NOT EXISTS consumers (
    `id`          BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'user id',
    -- `user_name`   VARCHAR(50)         NOT NULL UNIQUE COMMENT 'user name',
    `email`       VARCHAR(50)         NOT NULL UNIQUE COMMENT 'email',
    `password`    VARCHAR(64)         NOT NULL        COMMENT 'password',
    `industry`    VARCHAR(20)         NOT NULL        COMMENT 'industry',
    -- `created_at` timestamp with time zone default current_timestamp,
    `is_deleted`   BIT                 DEFAULT  0 COMMENT '1: deleted',
    -- deleted_at timestamp with time zone,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Initial data for test.
-- ----------------------------
START TRANSACTION;

INSERT INTO `consumers` VALUES
  (1, "1005871591@qq.com", "123456", "电网", 0),
  (2, "1005871592@qq.com", "223456", "负荷侧", 0);

COMMIT;