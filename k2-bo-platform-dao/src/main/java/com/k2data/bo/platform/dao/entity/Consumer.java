package com.k2data.bo.platform.dao.entity;

import java.io.Serializable;

/**
 * 消费者
 */
public class Consumer implements Serializable{
    private Long id;

    private String email;

    private String industry;

    private String password;

    private boolean isDeleted = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Consumer consumer = (Consumer) o;

        if (isDeleted != consumer.isDeleted) return false;
        if (id != null ? !id.equals(consumer.id) : consumer.id != null) return false;
        if (email != null ? !email.equals(consumer.email) : consumer.email != null) return false;
        if (industry != null ? !industry.equals(consumer.industry) : consumer.industry != null) return false;
        return password != null ? password.equals(consumer.password) : consumer.password == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (industry != null ? industry.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Consumer{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", industry='" + industry + '\'' +
                ", password='" + password + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
