package com.k2data.bo.platform.dao.mapper;

import com.k2data.bo.platform.dao.mapper.dynamic.sql.ConsumerSqlBuilder;
import com.k2data.bo.platform.dao.entity.Consumer;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ConsumerMapper {

    @SelectProvider(type = ConsumerSqlBuilder.class, method = "getOneOrList")
    @ResultMap("com.k2data.bo.platform.dao.mapper.ConsumerMapper.ConsumerMap")
    Consumer getOne(Consumer consumer);

    @SelectProvider(type = ConsumerSqlBuilder.class, method = "getOneOrList")
    @ResultMap("com.k2data.bo.platform.dao.mapper.ConsumerMapper.ConsumerMap")
    List<Consumer> getList(Consumer consumer);

    @Insert("INSERT INTO `consumers` VALUES (#{id}, #{email}, #{password}, #{industry}, #{isDeleted});")
    void insert(Consumer consumer);

    @DeleteProvider(type = ConsumerSqlBuilder.class, method = "delete")
    void delete(Consumer consumer);

}
