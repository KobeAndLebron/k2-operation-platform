package com.k2data.bo.platform.dao.mapper.dynamic.sql;

import com.k2data.bo.platform.dao.entity.Consumer;

public class ConsumerSqlBuilder {

    public String getOneOrList(Consumer consumer) {
        return consumer == null ? "select * from consumers;" : "select * from consumers " +
                "where id = #{id} " + (consumer.getPassword() == null ? "" : "and password = #{password};");
    }

    public String delete(Consumer consumer) {
        return "delete from consumers " +
                "where id = #{id} " + (consumer.getPassword() == null ? "" : "and password = #{password};");
    }

}
