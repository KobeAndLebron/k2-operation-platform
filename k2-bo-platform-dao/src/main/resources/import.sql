CREATE TABLE IF NOT EXISTS consumers (
    `id`          BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'user id',
    -- `user_name`   VARCHAR(50)         NOT NULL UNIQUE COMMENT 'user name',
    `email`       VARCHAR(50)         NOT NULL UNIQUE COMMENT 'email',
    `password`    VARCHAR(64)         NOT NULL        COMMENT 'password',
    `industry`    VARCHAR(20)         NOT NULL        COMMENT 'industry',
    -- `created_at` timestamp with time zone default current_timestamp,
    `is_deleted`   BIT                 DEFAULT  0 COMMENT '1: deleted',
    -- deleted_at timestamp with time zone,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Initial Data
-- ----------------------------
START TRANSACTION;

COMMIT;