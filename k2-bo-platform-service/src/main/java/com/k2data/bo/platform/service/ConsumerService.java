package com.k2data.bo.platform.service;

import com.k2data.bo.platform.dao.entity.Consumer;

public interface ConsumerService {

    Consumer getConsumerById(String id);

}
