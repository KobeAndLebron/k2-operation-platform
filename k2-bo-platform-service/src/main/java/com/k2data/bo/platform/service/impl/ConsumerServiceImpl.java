package com.k2data.bo.platform.service.impl;

import com.k2data.bo.platform.dao.mapper.ConsumerMapper;
import com.k2data.bo.platform.dao.entity.Consumer;
import com.k2data.bo.platform.service.ConsumerService;
import org.springframework.stereotype.Service;

@Service
public class ConsumerServiceImpl implements ConsumerService {

    private final ConsumerMapper USER_MAPPER;

    public ConsumerServiceImpl(ConsumerMapper user_mapper) {
        USER_MAPPER = user_mapper;
    }

    public Consumer getConsumerById(String id) {
        Consumer consumer = new Consumer();
        consumer.setId(Long.valueOf(id));
        return USER_MAPPER.getOne(consumer);
    }
}
