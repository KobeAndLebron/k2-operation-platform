<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.k2data.bo.platform</groupId>
    <artifactId>k2-bo-platform</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
        <module>k2-bo-platform-common</module>
        <module>k2-bo-platform-dao</module>
        <module>k2-bo-platform-service</module>
        <module>k2-bo-platform-api</module>
    </modules>

    <name>k2-bo-platform</name>
    <url>http://maven.apache.org</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <mybatis.spring.boot.version>1.3.2</mybatis.spring.boot.version>
        <mysql-driver.version>5.1.36</mysql-driver.version>
        <junit.version>4.12</junit.version>

        <spring.boot.version>1.5.7.RELEASE</spring.boot.version>

        <!-- Build -->
        <skip.ut>false</skip.ut>
        <skip.it>false</skip.it>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <!-- Import dependency management from Spring Boot -->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- Spring-MyBatis -->
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis.spring.boot.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter-test</artifactId>
                <version>${mybatis.spring.boot.version}</version>
                <scope>test</scope>
            </dependency>
            <!-- Spring-MyBatis -->
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- For test. -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- For test. -->
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <!-- add src/it/{java,resources} into source folder list -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>1.7</version>
                    <executions>
                        <execution>
                            <id>add-integration-test-sources</id>
                            <phase>pre-integration-test</phase>
                            <goals>
                                <goal>add-test-source</goal>
                            </goals>
                            <configuration>
                                <sources>
                                    <source>src/it/java</source>
                                </sources>
                            </configuration>
                        </execution>
                        <execution>
                            <id>add-integration-test-resources</id>
                            <phase>pre-integration-test</phase>
                            <goals>
                                <goal>add-test-resource</goal>
                            </goals>
                            <configuration>
                                <resources>
                                    <resource>
                                        <directory>src/it/resources</directory>
                                        <filtering>true</filtering>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <source>1.7</source>
                        <target>1.7</target>
                    </configuration>
                    <executions>
                        <execution>
                            <id>compile-integration-test</id>
                            <phase>pre-integration-test</phase>
                            <goals>
                                <goal>testCompile</goal>
                            </goals>
                            <configuration>
                                <testIncludes>
                                    <testInclude>**/*.java</testInclude>
                                </testIncludes>
                                <outputDirectory>${project.build.directory}/it-classes</outputDirectory>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.5</version>
                    <executions>
                        <execution>
                            <id>add-test-resources</id>
                            <phase>process-test-resources</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.build.directory}/test-classes</outputDirectory>
                                <resources>
                                    <resource>
                                        <directory>src/test/resources</directory>
                                        <filtering>true</filtering>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                        <execution>
                            <id>add-integration-test-resources</id>
                            <phase>pre-integration-test</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.build.directory}/it-classes</outputDirectory>
                                <resources>
                                    <resource>
                                        <directory>src/it/resources</directory>
                                        <filtering>true</filtering>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                        <execution>
                            <id>add-configuration-files</id>
                            <phase>process-resources</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.build.directory}/classes</outputDirectory>
                                <resources>
                                    <resource>
                                        <!-- 由于时间紧迫, 暂时采取此种方式 -->
                                        <!-- TODO Reference: https://stackoverflow.com/questions/17492622/
                                                specify-common-resources-in-a-multi-module-maven-project -->
                                        <directory>${project.basedir}/../k2-bo-platform-deploy/conf/log</directory>
                                        <filtering>true</filtering>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>2.6</version>
                    <configuration>
                        <excludeTransitive>false</excludeTransitive>
                        <outputDirectory>${project.build.directory}/lib</outputDirectory>
                        <stripVersion>true</stripVersion>
                    </configuration>
                    <executions>
                        <execution>
                            <id>copy-dependencies</id>
                            <phase>package</phase>
                            <goals>
                                <goal>copy-dependencies</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- 不需要在子POM中继承, 因为单元测试被绑定在Maven的默认生命周期 -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.18.1</version>
                    <configuration>
                        <skipTests>${skip.ut}</skipTests>
                        <testSourceDirectory>src/test/java</testSourceDirectory>
                        <includes>
                            <include>**/*Test.java</include>
                        </includes>
                        <excludes>
                            <exclude>**/*IT.java</exclude>
                        </excludes>
                        <testClassesDirectory>${project.build.directory}/test-classes</testClassesDirectory>
                    </configuration>
                </plugin>
                <!-- 如果子Module有集成测试的话， 那么这个插件就需要被继承 -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>2.18.1</version>
                    <configuration>
                        <skipTests>${skip.it}</skipTests>
                        <testSourceDirectory>src/it/java</testSourceDirectory>
                        <includes>
                            <include>**/*IT.java</include>
                        </includes>
                        <excludes>
                            <exclude>**/*Test.java</exclude>
                        </excludes>
                        <testClassesDirectory>${project.build.directory}/it-classes</testClassesDirectory>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>integration-test</goal>
                                <goal>verify</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <!-- TODO Plugin为什么不能继承？-->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring.boot.version}</version>
            </plugin>
        </plugins>
    </build>
</project>
